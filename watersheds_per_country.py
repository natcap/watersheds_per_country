"""Make a vector of shapefiles per country."""
import os
import logging
import sys
import zipfile
import glob

import ecoshard
import taskgraph
from osgeo import ogr
from osgeo import osr
from osgeo import gdal


WATERSHEDS_URL = r'https://storage.googleapis.com/global-invest-sdr-data/watersheds_globe_HydroSHEDS_15arcseconds_md5_c6acf2762123bbd5de605358e733a304.zip'
WORLD_BORDERS_URL = r'https://storage.googleapis.com/ecoshard-root/ipbes/TM_WORLD_BORDERS_SIMPL-0.3_md5_15057f7b17752048f9bd2e2e607fe99c.zip'

WORKSPACE_DIR = 'watersheds_per_country_workspace'
ECOSHARD_DIR = os.path.join(WORKSPACE_DIR, 'ecoshard')
CHURN_DIR = os.path.join(WORKSPACE_DIR, 'churn')
N_CPUS = -1
TASKGRAPH_REPORTING_RATE = 5.0

logging.basicConfig(
    level=logging.DEBUG,
    format=(
        '%(asctime)s (%(relativeCreated)d) %(levelname)s %(name)s'
        ' [%(funcName)s:%(lineno)d] %(message)s'),
    stream=sys.stdout)
LOGGER = logging.getLogger(__name__)
FH = logging.FileHandler('log.txt')
FH.setLevel(logging.DEBUG)
LOGGER.addHandler(FH)


def unzip_file(zipfile_path, target_dir, touchfile_path):
    """Unzip contents of `zipfile_path`.

    Parameters:
        zipfile_path (string): path to a zipped file.
        target_dir (string): path to extract zip file to.
        touchfile_path (string): path to a file to create if unzipping is
            successful.

    Returns:
        None.

    """
    with zipfile.ZipFile(zipfile_path, 'r') as zip_ref:
        zip_ref.extractall(target_dir)

    with open(touchfile_path, 'w') as touchfile:
        touchfile.write(f'unzipped {zipfile_path}')


def main():
    """Entry point."""
    for dir_path in [WORKSPACE_DIR, ECOSHARD_DIR, CHURN_DIR]:
        try:
            os.makedirs(dir_path)
        except OSError:
            pass

    task_graph = taskgraph.TaskGraph(
        WORKSPACE_DIR, N_CPUS, TASKGRAPH_REPORTING_RATE)

    for url in [WATERSHEDS_URL, WORLD_BORDERS_URL]:
        zipfile_path = os.path.join(ECOSHARD_DIR, os.path.basename(url))
        download_task = task_graph.add_task(
            func=ecoshard.download_url,
            args=(url, zipfile_path),
            target_path_list=[zipfile_path],
            task_name='download %s' % os.path.basename(url))

        touchfile_path = os.path.join(
            WORKSPACE_DIR, '%s.UNZIPPED' % os.path.splitext(
                os.path.basename(zipfile_path))[0])

        unzip_task = task_graph.add_task(
            func=unzip_file,
            args=(zipfile_path, CHURN_DIR, touchfile_path),
            dependent_task_list=[download_task],
            target_path_list=[touchfile_path],
            task_name='unzip %s' % zipfile_path)

    world_border_vector_path = os.path.join(
        CHURN_DIR, 'TM_WORLD_BORDERS-0.3.shp')
    watersheds_vector_dir_path = os.path.join(
        CHURN_DIR, 'watersheds_globe_HydroSHEDS_15arcseconds')

    world_border_vector = ogr.Open(world_border_vector_path)
    world_border_layer = world_border_vector.GetLayer()
    gpkg_driver = gdal.GetDriverByName('GPKG')

    wgs84_sr = osr.SpatialReference()
    wgs84_sr.ImportFromEPSG(4326)

    for country_feature in world_border_layer:
        country_name = country_feature.GetField('NAME').replace(' ', '_')
        country_geom = country_feature.GetGeometryRef()
        country_watershed_vector_path = os.path.join(
            WORKSPACE_DIR, '%s.gpkg' % country_name)
        LOGGER.debug('processing %s', country_name)

        country_watershed_vector = gpkg_driver.Create(
            country_watershed_vector_path, 0, 0, 0, gdal.GDT_Unknown)
        country_watershed_layer = country_watershed_vector.CreateLayer(
            os.path.splitext(os.path.basename(
                country_watershed_vector_path))[0],
            wgs84_sr, ogr.wkbPolygon)
        for watershed_vector_path in glob.glob(
                os.path.join(watersheds_vector_dir_path, '*.shp')):
            watershed_vector = gdal.OpenEx(
                watershed_vector_path, gdal.OF_VECTOR)
            watershed_layer = watershed_vector.GetLayer()
            watershed_layer.SetSpatialFilter(country_geom)
            for watershed_feature in watershed_layer:
                local_watershed_feature = ogr.Feature(
                    country_watershed_layer.GetLayerDefn())
                watershed_geom = watershed_feature.GetGeometryRef()
                local_watershed_feature.SetGeometry(watershed_geom.Clone())
                country_watershed_layer.CreateFeature(local_watershed_feature)
                watershed_geom = None
                watershed_feature = None
                local_watershed_feature = None
            watershed_layer = None
            watershed_vector = None
        country_watershed_layer = None
        country_watershed_vector = None

    task_graph.join()
    task_graph.close()


if __name__ == '__main__':
    main()
